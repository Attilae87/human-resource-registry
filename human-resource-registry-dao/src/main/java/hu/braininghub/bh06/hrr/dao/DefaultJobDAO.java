package hu.braininghub.bh06.hrr.dao;

import java.util.ArrayList;
import java.util.List;

import hu.braininghub.bh06.hrr.model.Job;

public class DefaultJobDAO extends DefaultBaseDAO<String, Job> implements JobDAO {

	public Job getJobByName(String name) {

		Job ret = null;
		List<Job> js = new ArrayList<Job>(entities.values());

		for (Job j : js) {

			if (name.equals(j.getTitle())) {
				ret = j;
				break;
			}
		}

		return ret;
	}

	@Override
	public IdGenerationStrategy<String> getIDGenerationStrategy() {
		return new UUIDGenerationStrategy();
	}

}
