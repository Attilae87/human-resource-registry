package hu.braininghub.bh06.hrr.dao;

import java.util.ArrayList;
import java.util.List;

import hu.braininghub.bh06.hrr.model.Employee;

public class DefaultEmployeeDAO extends DefaultBaseDAO<String, Employee> implements EmployeeDAO {

	public Employee getEmployeeByName(String name) {

		Employee ret = null;
		List<Employee> emps = new ArrayList<Employee>(entities.values());

		for (Employee e : emps) {

			if (name.equals(e.getFirstName())) {
				ret = e;
				break;
			}
		}

		return ret;
	}

	@Override
	public IdGenerationStrategy<String> getIDGenerationStrategy() {
		return new UUIDGenerationStrategy();
	}

}
