package hu.braininghub.bh06.hrr.dao;

import java.util.ArrayList;
import java.util.List;

import hu.braininghub.bh06.hrr.model.Department;

public class DefaultDepartmentDAO extends DefaultBaseDAO<String, Department> implements DepartmentDAO {

	public Department getDepartmentByName(String name) {

		Department ret = null;
		List<Department> ds = new ArrayList<Department>(entities.values());

		for (Department d : ds) {

			if (name.equals(d.getName())) {
				ret = d;
				break;
			}
		}

		return ret;
	}

	@Override
	public IdGenerationStrategy<String> getIDGenerationStrategy() {
		return new UUIDGenerationStrategy();
	}

}
