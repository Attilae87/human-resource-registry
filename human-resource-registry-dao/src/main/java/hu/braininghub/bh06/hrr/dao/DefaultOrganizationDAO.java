package hu.braininghub.bh06.hrr.dao;

import java.util.ArrayList;
import java.util.List;

import hu.braininghub.bh06.hrr.model.Organization;

public class DefaultOrganizationDAO extends DefaultBaseDAO<String, Organization> implements OrganizationDAO {

	public Organization getOrganizationByName(String name) {

		Organization ret = null;
		List<Organization> orgs = new ArrayList<Organization>(entities.values());

		for (Organization o : orgs) {

			if (name.equals(o.getOrganizationName())) {
				ret = o;
				break;
			}
		}

		return ret;
	}

	@Override
	public IdGenerationStrategy<String> getIDGenerationStrategy() {
		return new UUIDGenerationStrategy();
	}

}
